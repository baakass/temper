<?php
namespace App\Module;

use App\Module\RetriveFromCsv;
use App\Module\Calculate;

class Chart 
{
    
    public function getDataForChatrs()
    {
    	// for this case data retriveing from the csv file. so we call folowing class
    	$getData = new RetriveFromCsv();
    	$userDataSet = $getData->getDataFromSource();

    	// calculate user details for genarate charts
    	$calculateData = new Calculate();
    	$calculatedData = $calculateData->calculate($userDataSet);

    	return $calculatedData; 
    	
    }

}