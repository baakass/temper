<?php
namespace App\Module;

use App\Module\RetrivingData;

/**
 * this class use to get data from the csv
 */
class RetriveFromCsv implements RetrivingData
{
    
    public function getDataFromSource()
    {
    	$userDetails = [];
    	$file = fopen("../dataSet.csv","r");
    	$count  = 0;
		while(! feof($file))
		  {
		  	$tempSingleRecordSet = fgetcsv($file);
		  	if ($count != 0) {
		  		$userDetails[] = [
		  			'userID' => $tempSingleRecordSet[0],
		  			'createdDate' => $tempSingleRecordSet[1],
		  			'onboardPerentage' => $tempSingleRecordSet[2],
		  			'countApplications' => $tempSingleRecordSet[3],
		  			'accepted' => $tempSingleRecordSet[4]
		  		];
		  	}
		  	$count++;
		  }
		fclose($file);

		return $userDetails;
		
    }

}