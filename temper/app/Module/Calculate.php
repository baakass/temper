<?php
namespace App\Module;

/**
* this class use to calculate all the details that related to the chart
**/
class Calculate 
{
    
    public function calculate($data)
    {
    	$respondData = $this->filterAsWeekly($data);
    	$respondOfAvgValues = $this->findDataAvgForEachWeeks($respondData);
    	$respondOfAvgValues = $this->setPercentageForGivenSteps($respondOfAvgValues);
    	
    	return $respondOfAvgValues; 
    }

    public function filterAsWeekly($data)
    {
    	$counter = 0;
    	$lowestTime = 0; // no need to sort. because given data set already sorted...
    	$totalValueOFWeek = 604800; // in seconds
    	$weeklySorted = [];
    	foreach ($data as $key => $value) {
    		
    		if ($counter == 0) {
    			$lowestTime = date(strtotime($value['createdDate']));
    		}
    		
    		if (floatval(date(strtotime($value['createdDate']))) - floatval($lowestTime) < floatval ($totalValueOFWeek)) {
    			$weeklySorted[date('Y/m/d',$lowestTime)][] = $value;
    		} else {

    			$week = intdiv((date(strtotime($value['createdDate']))-floatval($lowestTime)), floatval($totalValueOFWeek));
    			$totalSecondsForNewWeek = $week * floatval($totalValueOFWeek);
    			$newWeekDate = floatval($totalSecondsForNewWeek) + floatval($lowestTime);
    			$weeklySorted[date('Y/m/d',$newWeekDate)][] = $value;
    		}		
    		$counter++;
    	}
    	return $weeklySorted;
    	
    }

    public function findDataAvgForEachWeeks($dataSet)
    {
    	$weeklySortedDataSet = [];
    	foreach ($dataSet as $dataKey => $dataValue) {
    		$cusCount = 1;
	    	foreach ($dataValue as $key => $value) {
	    		$weeklySortedDataSet[$dataKey]['percentage'][$value['onboardPerentage']] = (!empty($weeklySortedDataSet[$dataKey]['percentage'][$value['onboardPerentage']])) ? $weeklySortedDataSet[$dataKey]['percentage'][$value['onboardPerentage']]+ 1 : 1;
	    		$cusCount++;
	    	}
	    	$weeklySortedDataSet[$dataKey]['customerCount'] = $cusCount;
    	}

    	return $weeklySortedDataSet; 
    }

    public function setPercentageForGivenSteps($data)
    {
    	$finalDataSet = [];
    	foreach ($data as $dataKey => $value) {
    		$keys = array(0, 5, 10, 15,20,25,30,35,40,45,50,55,60,65,70,75,80,85,90,99,100);
			$tmpArray = array_fill_keys($keys, 0);
			$tmpArray[0] = 100;
    		foreach ($value['percentage'] as $secondKey => $secondValue) {
    			if ($secondKey != '') {
    				$tmpArray[$secondKey] = (floatval($secondValue)/floatval($value['customerCount']))*100;
    			}
    		}
    		$finalDataSet[$dataKey] = $tmpArray;
    	}

    	return $finalDataSet;
    }

}