<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use App\Module\Chart;

class ChartController extends Controller
{
    /**
    * this function use to get chart data for temper front end
    **/
    public function show()
    {
        $chart = new Chart();
        $respond = $chart->getDataForChatrs();
        
        return json_encode($respond);
    }
}