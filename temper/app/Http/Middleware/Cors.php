<?php

namespace App\Http\Middleware;

use Closure;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $allowDomains = env('ALLOW_DOMAINS');
         $domains = explode(",", $allowDomains);
         
         if (isset($request->server()['HTTP_ORIGIN'])) {
             $origin = $request->server()['HTTP_ORIGIN'];
             if (in_array($origin, $domains)) {
                 header('Access-Control-Allow-Origin: ' . $origin);
                 header('Content-Type: application/json');
                 header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
                 header('Access-Control-Allow-Headers: Origin, Content-Type, Authorization, X-Requested-With, X-HTTP-Method-Override, Accept, X-Location, X-Test-Mode, X-Wizard');
                 header('Access-Control-Expose-Headers:newToken');
             }
         }
         return $next($request);
    }
}
